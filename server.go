package gopher

import (
	"bufio"
	"net"
	"strings"
)

// Handler is something that can handle a client connection.
type Handler interface {
	Serve(writer *ResponseWriter, request *Request)
} //interface

// HandlerFunc is a function that implements Handler.
type HandlerFunc func(*ResponseWriter, *Request)

// Serve the connection by using the HandlerFunc.
func (h HandlerFunc) Serve(w *ResponseWriter, r *Request) {
	h(w, r)
} //func

// Server takes care of client connections.
type Server struct {
	Addr    string
	Handler Handler
} //struct

// ListenAndServe starts the server listening and handling connections.
func (s *Server) ListenAndServe() error {
	// Create the listener
	listener, err := net.Listen("tcp", s.Addr)

	// Check for an error
	if err != nil {
		return err
	} //if

	return s.Serve(listener)
} //func

// Serve starts the server handling connections/requests with the provided
// listener.
func (s *Server) Serve(listener net.Listener) error {
	// Start accepting client connections
	for {
		// Accept a connection
		client, err := listener.Accept()

		// Check for an error
		if err != nil {
			// Not sure what to do here
			continue
		} //if

		// Handle the client
		go s.handle(client)
	} //for

	return nil
} //func

func (s *Server) handle(conn net.Conn) {
	// Close the connection later
	defer conn.Close()

	// Create a line reader
	r := bufio.NewReader(conn)

	// Read a line
	line, err := r.ReadString('\n')

	// Check for an error
	if err != nil {
		return
	} //if

	// Trim space from the line
	line = strings.TrimSpace(line)

	// Create the request
	req := Request{
		Path: line,
		Addr: conn.RemoteAddr(),
	}

	// Create the client
	resp := ResponseWriter{
		out: conn,
	}

	// Handle the connection
	s.Handler.Serve(&resp, &req)
} //func

// ListenAndServe is a conveince methods to quickly setup a Server and start
// handling requests.
func ListenAndServe(addr string, handler Handler) error {
	// Create a server
	server := Server{
		Addr:    addr,
		Handler: handler,
	}

	// Start the server
	return server.ListenAndServe()
} //func
