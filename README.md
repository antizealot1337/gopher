# Gopher
[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/gopher?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/gopher)

A library to assit in creating gopher servers and (eventually) clients. This
library is heavily inspired by the Go standard library's `net/http` with many
design choices and concessions made to better support the gopher protocol.

## Status
Currently, this project is in the early development stage and shouldn't even be
considered __alpha__ level software yet. None of the API should be considered
stable. *THIS IS NOT PRODUCTION READY*.

## Why?
Who doesn't want a Go powered gopher server?

## Usage
*__TODO__*

## License
Licensed under the terms of the MIT license. See LICENSE file for more
information.
