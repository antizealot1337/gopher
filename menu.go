package gopher

import "path"

// ItemType represents a menu item type for the Gopher protocol.
type ItemType byte

// Canonical types
const (
	ItemText           ItemType = '0'
	ItemSubMenu        ItemType = '1'
	ItemCCSONameserver ItemType = '2'
	ItemError          ItemType = '3'
	ItemBinHexEnc      ItemType = '4'
	ItemDOSFile        ItemType = '5'
	ItemUuencoded      ItemType = '6'
	ItemFullTextSearch ItemType = '7'
	ItemTelnet         ItemType = '8'
	ItemBinary         ItemType = '9'
	ItemMirror         ItemType = '+'
	ItemGIF            ItemType = 'g'
	ItemImage          ItemType = 'I'
)

// Non-canonical types
const (
	ItemHTML    ItemType = 'h'
	ItemInfoMsg ItemType = 'i'
	ItemSound   ItemType = 's'
)

// Item represents an item in a gopher menu.
type Item struct {
	Type     ItemType
	Display  string
	Selector string
	Hostname string
	Port     int
} //func

// Menu is a slice of Items.
type Menu []Item

// GuessItemType will attempt to deduce the ItemType by looking at the provided
// filename. Non-canonical types are included. For Canonical only types use
// CannonicalGuessItemType. The rules are as follows:
//
// If the file ends in .txt it is assumed to be a text file.
// If it ends in .gif it is a GIF.
// Files ending in .png, .jp(e)?g, ,.svg, .tiff are assumed to be image files.
// If the file ends in .html it is assumed to be a HTML file.
// Files ending with .wav, .mp3, .aac, .flac, .ogg are assumed to be sound files.
func GuessItemType(name string) ItemType {
	switch ext := path.Ext(name); ext {
	case ".txt":
		return ItemText
	case ".gif":
		return ItemGIF
	case ".png", ".jpeg", ".jpg", ".svg", ".tiff":
		return ItemImage
	case ".html":
		return ItemHTML
	case ".wav", ".mp3", ".aac", ".flac", ".ogg":
		return ItemSound
	default:
		return ItemBinary
	} //switch
} //func

// CannonicalGuessItemType will attempt to deduce the ItemType by looking at
// the provided name. It follows the same rules as GuessItemType but will
// return ItemBinary for HTML and sound/audio files.
func CannonicalGuessItemType(name string) ItemType {
	switch typ := GuessItemType(name); typ {
	case ItemHTML, ItemSound:
		return ItemBinary
	default:
		return typ
	} //swtich
} //if
