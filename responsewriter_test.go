package gopher

import (
	"bytes"
	"testing"
)

func TestResponseWriterWriteInfo(t *testing.T) {
	// Create the buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	rw := ResponseWriter{
		out: buff,
	}

	// Write some info
	err := rw.WriteInfo("message")

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	expected := "imessage\t(NULL)\t(NULL)\t0\r\n"

	if actual := buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but got "%s"`, expected, actual)
	} //if
} //func

func TestResponseWriterWriteError(t *testing.T) {
	// Create the buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	rw := ResponseWriter{
		out: buff,
	}

	// Write some info
	err := rw.WriteError("message")

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	expected := "3message\t(NULL)\t(NULL)\t0\r\n"

	if actual := buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but got "%s"`, expected, actual)
	} //if
} //func

func TestResponseWriterWriteLink(t *testing.T) {
	// Create the buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	rw := ResponseWriter{
		out: buff,
	}

	// Write some info
	err := rw.WriteLink("message", "/message", "example.org", 70)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	expected := "1message\t/message\texample.org\t70\r\n"

	if actual := buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but got "%s"`, expected, actual)
	} //if
} //func

func TestResponseWriterWriteText(t *testing.T) {
	// Create the buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	rw := ResponseWriter{
		out: buff,
	}

	// Write some info
	err := rw.WriteText("message", "/message", "example.org", 70)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	expected := "0message\t/message\texample.org\t70\r\n"

	if actual := buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but got "%s"`, expected, actual)
	} //if
} //func

func TestResponseWriterWriteBin(t *testing.T) {
	// Create the buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	rw := ResponseWriter{
		out: buff,
	}

	// Write some info
	err := rw.WriteBin("message", "/message", "example.org", 70)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	expected := "9message\t/message\texample.org\t70\r\n"

	if actual := buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but got "%s"`, expected, actual)
	} //if
} //func

func TestResponseWriterRawItem(t *testing.T) {
	// Create the buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	iw := ResponseWriter{
		out: buff,
	}

	// Write an item
	err := iw.RawItem('1', "a", "b", "c", 2)

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check what was written
	if expected, actual := "1a\tb\tc\t2\r\n", buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func

func TestResponseWriterDone(t *testing.T) {
	// Create a buffer
	buff := bytes.NewBuffer(nil)

	// Create an ResponseWriter
	iw := ResponseWriter{
		out: buff,
	}

	// Write done
	err := iw.Done()

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Check what was written
	if expected, actual := ".\r\n", buff.String(); actual != expected {
		t.Errorf(`Expected "%s" but was "%s"`, expected, actual)
	} //if
} //func
