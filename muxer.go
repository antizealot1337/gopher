package gopher

import "strings"

// Muxer handles looks at the request and determines which Handler to call.
type Muxer struct {
	handlers map[string]Handler
} //func

// NewMuxer returns a new Muxer.
func NewMuxer() *Muxer {
	return &Muxer{
		handlers: make(map[string]Handler),
	} //struct
} //func

// Serve the client with some Handler.
func (m *Muxer) Serve(w *ResponseWriter, r *Request) {
} //func

// Handle adds a new Handler for the path to the Muxer. If a previous Handler
// existed for the path it is forgotten.
func (m *Muxer) Handle(path string, handler Handler) {
	m.handlers[cleanPath(path)] = handler
} //func

// cleanPath removes any "/" from the start of the handle path.
func cleanPath(path string) string {
	return strings.TrimLeftFunc(path, func(c rune) bool { return c == '/' })
} //func
