package gopher

import (
	"bytes"
	"io"
	"net"
	"testing"
)

func TestServerServe(t *testing.T) {
	// Create a server
	srv := Server{
		Handler: HandlerFunc(func(w *ResponseWriter, r *Request) {
			if expected, actual := "", r.Path; actual != expected {
				t.Errorf(`Expected path "%s" but was"%s"`,
					expected, actual)
			} //if
			w.RawItem(byte(ItemInfoMsg), "hello world", "(NULL)", "(NULL)", 0)
			w.Done()
		}), //func
	} //struct

	// Create a listener
	l, err := net.Listen("tcp", ":")

	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //f

	// Close the listener later
	defer l.Close()

	// Serve the listener
	go srv.Serve(l)

	// Make a request
	conn, err := net.Dial("tcp", l.Addr().String())

	// Check for an error
	if err != nil {
		t.Fatal("Unexpected error:", err)
	} //if

	// Probably net strictly needed but be nice to the OS ;-)
	defer conn.Close()

	conn.Write([]byte("\r\n"))

	// Create a buffer to store the response
	buff := bytes.NewBuffer(nil)

	io.Copy(buff, conn)

	expected := "ihello world\t(NULL)\t(NULL)\t0\r\n.\r\n"

	if actual := buff.String(); actual != expected {
		t.Error("Expected\n", expected)
		t.Error("Got\n", actual)
	} //if
} //func
