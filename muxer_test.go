package gopher

import "testing"

var _ Handler = (*Muxer)(nil)

func TestNewMuxer(t *testing.T) {
	// Create a new muxer
	mux := NewMuxer()

	// Make sure the muxer isn't nil
	if mux == nil {
		t.Fatal("Expected muxer to be returned")
	} //if

	// Make sure the handlers map was initialized
	if mux.handlers == nil {
		t.Fatal("Expected handlers to be initialized")
	} //if
} //func

func TestMuxerServe(t *testing.T) {
} //func

func TestMuxerHandle(t *testing.T) {
	// Creat a new muxer
	mux := NewMuxer()

	// Create a Handler
	mux.Handle("", HandlerFunc(func(w *ResponseWriter, r *Request) {
	}))

	// Make sure the handlers have increase in number
	if expected, actual := 1, len(mux.handlers); actual != expected {
		t.Errorf("Expected handlers count to be %d but was %d", expected, actual)
	} //if

	// Create a Handler
	mux.Handle("", HandlerFunc(func(w *ResponseWriter, r *Request) {
	}))

	// Make sure the handlers have increase in number
	if expected, actual := 1, len(mux.handlers); actual != expected {
		t.Errorf("Expected handlers count to be %d but was %d", expected, actual)
	} //if

} //func

func TestCleanPath(t *testing.T) {
	if expected, actual := "", cleanPath("/"); actual != expected {
		t.Errorf(`Expected path to be "%s" but was "%s"`, expected, actual)
	} //if

	if expected, actual := "bob/1", cleanPath("//bob/1"); actual != expected {
		t.Errorf(`Expected path to be "%s" but was "%s"`, expected, actual)
	} //if
} //func
