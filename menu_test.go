package gopher

import "testing"

func TestGuessItemType(t *testing.T) {
	// Function to check the item type
	checkItemType := func(name string, typ ItemType) {
		// Guess the type and check it
		if atyp := GuessItemType(name); typ != atyp {
			t.Errorf(`Expected type %c from "%s" but was %c`,
				typ, name, atyp)
		} //if
	} //func

	checkItemType("a.txt", ItemText)
	checkItemType("a.gif", ItemGIF)
	checkItemType("a.png", ItemImage)
	checkItemType("a.jpeg", ItemImage)
	checkItemType("a.jpg", ItemImage)
	checkItemType("a.svg", ItemImage)
	checkItemType("a.tiff", ItemImage)
	checkItemType("a.html", ItemHTML)
	checkItemType("a.wav", ItemSound)
	checkItemType("a.mp3", ItemSound)
	checkItemType("a.aac", ItemSound)
	checkItemType("a.flac", ItemSound)
	checkItemType("a.ogg", ItemSound)
	checkItemType("a.zip", ItemBinary)
} //func

func TestCannonicalGuessItemType(t *testing.T) {
	// Function to check the item type
	checkItemType := func(name string, typ ItemType) {
		// Guess the type and check it
		if atyp := CannonicalGuessItemType(name); typ != atyp {
			t.Errorf(`Expected type %c from "%s" but was %c`,
				typ, name, atyp)
		} //if
	} //func

	checkItemType("a.html", ItemBinary)
	checkItemType("a.wav", ItemBinary)
	checkItemType("a.mp3", ItemBinary)
	checkItemType("a.aac", ItemBinary)
	checkItemType("a.flac", ItemBinary)
	checkItemType("a.ogg", ItemBinary)
} //func
