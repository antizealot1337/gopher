package gopher

import (
	"fmt"
	"io"
)

// ResponseWriter provides functionality to write to a Gopher client. It provides somme
// convenience methods for writing "menu items".
type ResponseWriter struct {
	out io.Writer
} //struct

// Write data to the ResponseWriter.
func (w *ResponseWriter) Write(data []byte) (int, error) {
	return w.out.Write(data)
} //func

// WriteMenu writes a Menu.
func (w *ResponseWriter) WriteMenu(m Menu) (err error) {
	// Loop through the items
	for _, item := range m {
		// Write the item
		err = w.WriteItem(item)

		// Check for an error
		if err != nil {
			return fmt.Errorf("menu writer error: %v", err)
		} //if
	} //for

	return
} //for

// WriteItem writes an Item from a Menu.
func (w *ResponseWriter) WriteItem(i Item) error {
	return w.RawItem(byte(i.Type), i.Display, i.Selector, i.Hostname, i.Port)
} //func

// WriteInfo writes an info line.
func (w *ResponseWriter) WriteInfo(msg string) error {
	return w.RawItem(byte(ItemInfoMsg), msg, "(NULL)", "(NULL)", 0)
} //func

// WriteError writes an error line.
func (w *ResponseWriter) WriteError(msg string) error {
	return w.RawItem(byte(ItemError), msg, "(NULL)", "(NULL)", 0)
} //if

// WriteLink writes a link to gopher submenu.
func (w *ResponseWriter) WriteLink(disp, sel, hname string, port int) error {
	return w.RawItem(byte(ItemSubMenu), disp, sel, hname, port)
} //func

// WriteText writes a text link item.
func (w *ResponseWriter) WriteText(disp, sel, hname string, port int) error {
	return w.RawItem(byte(ItemText), disp, sel, hname, port)
} //func

// WriteBin writes a binary link item.
func (w *ResponseWriter) WriteBin(disp, sel, hname string, port int) error {
	return w.RawItem(byte(ItemBinary), disp, sel, hname, port)
} //func

// RawItem is a method that does a raw write of an item withough any
// checking. Use other methods for specific (known) types.
func (w *ResponseWriter) RawItem(kind byte, disp, sel, hname string, port int) (err error) {
	// Write the item line
	_, err = fmt.Fprintf(w, "%c%s\t%s\t%s\t%d\r\n",
		kind,
		disp,
		sel,
		hname,
		port)

	// Check for an error
	if err != nil {
		err = fmt.Errorf("item writer error: %v", err)
	} //if

	return
} //func

// Done will send a full stop (".") on a line all to itself. A "good" gopher
// server will write a full stop before disconnecting the client. However, most
// clients seem to be able to deal with a missing full-stop. Thus calling Done
// is probably optional (but recommended).
func (w *ResponseWriter) Done() (err error) {
	// Write the full stop
	_, err = fmt.Fprint(w, ".\r\n")

	// Check for an error
	return
} //func
