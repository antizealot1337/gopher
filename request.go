package gopher

import "net"

// Request contains data about a gopher client request.
type Request struct {
	Path string
	Addr net.Addr
} //struct
